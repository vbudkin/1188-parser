$(function() {
    $(document).on("click", '.save-firm', function() {
        var obj = $(this);
        var data = {
            link: obj.attr('data-link'),
            category: obj.attr('data-category')
        };
        obj.attr('disabled',true);
        $('.table').css('opacity','.3');
        $.post(base + 'main/parse_firm', data, function() {
           
        }).done(function() {
           obj.closest('tr').find('span.glyphicon-remove').removeClass('glyphicon-remove text-danger').addClass('glyphicon-ok text-success');
           $('.table').css('opacity','1');
           obj.remove();
        });
    });
});