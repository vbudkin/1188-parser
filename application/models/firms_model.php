<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Firms_model extends BF_Model {

    protected $table_name = 'firms';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $set_created = FALSE;
    protected $set_modified = FALSE;

    public function __construct() {
        
    }

    public function parse_firms_by_category($link) {
        $return = [
            'total' => 0,
            'data' => []
        ];
        $html = $this->curl->simple_get($link);
        $dom = $this->dom_parser->str_get_html($html);
        $total = $dom->find('span[data-action=show/totall]');
        if ($total) {
            $return['total'] = trim($total[0]->plaintext);
        }

        foreach ($dom->find('a[class=title]') as $key => $value) {
            if (isset($value->attr['hreflang'])) {
                $return['data'][] = [
                    'title_1188' => trim($value->plaintext),
                    'link_1188' => 'http://www.1188.lv' . $value->attr['href'],
                    'id_1188' => end(explode('/', $value->attr['href']))
                ];
            }
        }
        return $return;
    }

    public function firm_ids_by_category($category_id) {
        $return = [];
        $result = $this->db
                ->select('f.id_1188')
                ->join('firms f', 'f.id = fc.firms_id', 'left')
                ->get_where('firms_categories fc', ['fc.categories_id' => $category_id])
                ->result();
        if ($result) {
            foreach ($result as $value) {
                $return[] = $value->id_1188;
            }
        }
        return $return;
    }

    public function parse_firm($link) {
        $html = $this->curl->simple_get($link);
        $dom = $this->dom_parser->str_get_html($html);
        $return = [
            'id_1188' => end(explode('/', $link)),
            'link_1188' => $link,
            'title_1188' => null,
            'phone' => null,
            'email' => null,
            'home_page' => null,
            'address' => null,
        ];
        
        $return['title_1188'] = trim($dom->find('h1[class=title]',0)->find('span',0)->plaintext);
        
        $return['address'] = $dom->find('div[class=address]',0)->innertext;
        $return['address'] = preg_replace('/<div[^>]*>([\s\S]*?)<\/div[^>]*>/', '', $return['address']);
        $return['address'] = preg_replace('/<form[^>]*>([\s\S]*?)<\/form[^>]*>/', '', trim($return['address']));
        
        $items = $dom->find('ul[class=items]',0);
        
        foreach ($items->find('a') as $value) {
            if (isset($value->attr['rel']) && filter_var(trim($value->plaintext), FILTER_VALIDATE_EMAIL)) {
                $return['email'] = trim($value->plaintext);
            }
            if (isset($value->attr['href']) && filter_var($value->attr['href'], FILTER_VALIDATE_URL)) {
                $return['home_page'] = $value->attr['href'];
            }
        }
        
        foreach ($items->find('a[class=number]') as $value) {
            $return['phone'] = $value->plaintext;
        }

        return $return;
    }

    public function save_firm($data) {
        $exist = $this->select('id')->find_by('id_1188', $data['id_1188']);
        return $exist ? $exist->id : $this->insert($data);
    }

    public function get_firms($limit = false) {
        $params = $this->input->get();
        $limit ? $this->db->select('f.*') : $this->db->select('f.id');
        if ($limit) {
            $this->db->limit($limit, $this->uri->rsegment(3));
        }
        if ($params['category']) {
            $this->db->join('firms_categories fc', 'f.id = fc.firms_id', 'left');
            $this->db->where(['fc.categories_id' => $params['category']]);
        }
        if ($params['word']) {
            $this->search_like_gen($params['word']);
        }
        $query = $this->db->get('firms f');
        return $limit ? $query->result() : $query->num_rows();
    }

    public function search_like_gen($word) {
        $this->db->like('f.title_1188', $word);
        $this->db->or_like('f.phone', $word);
        $this->db->or_like('f.email', $word);
    }

    public function list_fields() {
        return $this->db->list_fields($this->table_name);
    }

    public function get_exel_data($params) {
        isset($params['db_fields']) ? $this->db->select($params['db_fields']) : $this->db->select('*');
        if ($params['category']) {
            $this->db->join('firms_categories fc', 'f.id = fc.firms_id', 'left');
            $this->db->where(['fc.categories_id' => $params['category']]);
        }
        return $this->db->get('firms f')->result();
    }

    public function get_firms_links_by_category($category_link, $firms_ids) {
        $return = [];
        $parse_data = $this->parse_firms_by_category($category_link);
        foreach ($parse_data['data'] as $key => $value) {
            if (!in_array($value['id_1188'], $firms_ids)) {
                $return[] = $value['link_1188'];
            }
        }
        $pages = (int) ceil($parse_data['total'] / 20);
        if ($pages > 1) {
            for ($i = 2; $i <= $pages; $i++) {
                sleep(rand(2, 5));
                $parse_data = $this->parse_firms_by_category($category_link . '?page=' . $i);
                foreach ($parse_data['data'] as $key => $value) {
                    if (!in_array($value['id_1188'], $firms_ids)) {
                        $return[] = $value['link_1188'];
                    }
                }
            }
        }
        return $return;
    }

}
