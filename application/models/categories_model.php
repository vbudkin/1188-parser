<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories_model extends BF_Model {

    protected $table_name = 'categories';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $set_created = FALSE;
    protected $set_modified = FALSE;
    public $parse_category_link = 'http://www.1188.lv/katalogs';

    public function __construct() {
        
    }

    public function parse_categories() {
        $html = $this->curl->simple_get($this->parse_category_link);
        $dom = $this->dom_parser->str_get_html($html);
        $return = [];
        foreach ($dom->find('div[class=letter-content]') as $key => $value) {
            foreach ($value->find('a') as $k => $v) {
                $return[] = [
                    'title' => $v->plaintext,
                    'link' => 'http://www.1188.lv' . $v->attr['href']
                ];
            }
        }
        return $return;
    }

    public function save_parsed_categories($data) {
        $insert = [];
        $update = [];
        foreach ($data as $key => $value) {
            $exist = $this->select('id')->find_by('title', $value['title']);
            if ($exist) {
                $update[] = [
                    'id' => $exist->id,
                    'title' => $value['title'],
                    'link' => $value['link'],
                ];
            } else {
                $insert[] = [
                    'title' => $value['title'],
                    'link' => $value['link'],
                ];
            }
        }
        if ($insert) {
            $this->insert_batch($insert);
        }

        if ($update) {
            $this->update_batch($update, 'id');
        }
    }

    public function categories_dropdown($all = true) {
        $return = [
            0 => 'Выберите категорию'
        ];
        $this->db->select('c.id,c.title');
        if(!$all){
            $this->db->join('firms_categories fc','c.id = fc.categories_id');
            $this->db->group_by('c.id');
        }
        $this->db->order_by('title');
        $result = $this->db->get('categories c')->result();
        if ($result) {
            foreach ($result as $value) {
                $return[$value->id] = $value->title;
            }
        }
        return $return;
    }

    public function save_relation($firm_id, $category_id) {
        $exist = $this->db->get_where('firms_categories', ['firms_id' => $firm_id, 'categories_id' => $category_id])->result();
        if (!$exist) {
            $this->db->insert('firms_categories', ['firms_id' => $firm_id, 'categories_id' => $category_id]);
        }
    }

}
