<?php $this->load->view('components/header'); ?>
<?php $this->load->view('components/navigation'); ?>
<div class="container">
    <?php if (file_exists(APPPATH . 'views/' . $this->router->class . '/sub_nav' . EXT)): ?>
	<?php $this->load->view($this->router->class . '/sub_nav'); ?>
    <?php endif; ?>
    <?php $this->load->view('components/msg'); ?>
    <?php $this->load->view($this->data['main']); ?>
</div>
<?php $this->load->view('components/footer'); ?>