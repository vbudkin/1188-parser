<?php
$msg = array();
$alert_type = false;
$output = false;
$msg['success'] = $this->session->flashdata('success');
$msg['error'] = $this->session->flashdata('error');
$msg['warning'] = $this->session->flashdata('warning');
$msg['info'] = $this->session->flashdata('info');
if ($this->session->flashdata('success')) {
    $alert_type = 'success';
    $output = $this->session->flashdata('success');
} else if ($this->session->flashdata('error')) {
    $alert_type = 'danger';
    $output = $this->session->flashdata('error');
} else if ($this->session->flashdata('warning')) {
    $alert_type = 'warning';
    $output = $this->session->flashdata('warning');
} else if ($this->session->flashdata('info')) {
    $alert_type = 'info';
    $output = $this->session->flashdata('info');
}
?>
<?php if ($alert_type && $output): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-<?php echo $alert_type; ?> alert-dismissable text-center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $output; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
