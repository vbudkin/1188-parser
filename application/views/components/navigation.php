<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo anchor(base_url(), '1188 parser','class="navbar-brand"')?>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php $active = $this->uri->rsegment(1) == 'main' ?  'class="active"' :  null;?>
                <li <?php echo $active?>><?php echo anchor('/', 'Парсер')?></li>
                <?php $active = $this->uri->rsegment(1) == 'firms' ?  'class="active"' :  null;?>
                <li <?php echo $active?>><?php echo anchor('firms', 'Фирмы')?></li>
                <?php $active = $this->uri->rsegment(1) == 'categories' ?  'class="active"' :  null;?>
                <li <?php echo $active?>><?php echo anchor('categories', 'Категории')?></li>
            </ul>
        </div>
    </div>
</nav>