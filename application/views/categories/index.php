<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">Обзор и управление категориями</div>
                <div class="pull-right"><?php echo anchor('categories/parse', 'Спарсить категории', 'class="btn btn-xs btn-success"'); ?></div>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>Название</th>
                        <th class="text-right">Опции</th>
                    </tr>
                    <?php if ($records): ?>
                        <?php foreach ($records as $key => $value): ?>
                            <tr>
                                <td><?php echo $value->title;?></td>
                                <td class="text-right">
                                    <?php echo anchor('categories/delete/'.$value->id,'<span class="glyphicon glyphicon-trash"></span>','class="btn btn-xs btn-danger" onclick="return confirm(\'Вы действительно хотите удалить запись?\')"');?>
                                    <?php echo anchor($value->link,'<span class="glyphicon glyphicon-eye-open"></span>','class="btn btn-xs btn-info" target="_blank"');?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="2" class="bg-warning text-center">Категории не найдены</td></tr>
                    <?php endif; ?>
                </table>
            </div>
            <div class="panel-footer">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
