<ul class="nav nav-tabs">
    <?php $active = $this->uri->rsegment(2) == 'index' ? 'class="active"' : null; ?>
    <li <?php echo $active ?>><?php echo anchor('firms', 'Обзор') ?></li>
    <?php $active = $this->uri->rsegment(2) == 'export' ? 'class="active"' : null; ?>
    <li <?php echo $active ?>><?php echo anchor('firms/export', 'Экспорт') ?></li>
</ul>
