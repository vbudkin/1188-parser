<div class="row">
    <div class="col-lg-12">
        <?php echo form_open('firms/export_xsl', ['method' => 'get']); ?>
        <div class="panel panel-default">
            <div class="panel-heading clearfix">Экспорт данных</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Выберите категорию</label>
                            <?php echo form_dropdown('category', $categories, 0, 'class="form-control"'); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label>Выберите данные для экспорта</label>
                        <ul style="list-style: none;padding: 0;">
                        <?php foreach ($db_fields as $key => $value):?>
                            <li>
                                <?php echo form_checkbox(['name'=>'db_fields[]','value'=>$value,'id'=>$value]);?> 
                                <label style="cursor: pointer" for="<?php echo $value;?>"><?php echo $value;?></label>
                            </li>
                        <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-footer"><?php echo form_submit('submit', 'Скачать excel', 'class="btn btn-success"'); ?></div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>