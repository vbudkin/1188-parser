<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="navbar-brand">Поиск по фирмам</span>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <?php echo form_open('firms/index', ['method' => 'get', 'id' => 'search-firms', 'class' => 'navbar-form']); ?>
        <div class="navbar-left">
            <div class="form-group">
                <?php echo form_dropdown('category', $categories, $this->input->get('category'), 'class="form-control"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_input('word', set_value('word', $this->input->get('word')), 'class="form-control" placeholder="Название, телефон, email"'); ?>
            </div>
        </div>
        <div class="navbar-right"> 
            <button type="submit" class="btn btn-default">Найти</button>
        </div>
        <?php echo form_close(); ?>
    </div>
</nav>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">Результаты поиска</div>
                <div class="pull-right"></div>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>Название</th>
                        <th>Эмаил</th>
                        <th>Телефон</th>
                        <th>Адрес</th>
                        <th class="text-right">Опции</th>
                    </tr>
                    <?php if ($records): ?>
                        <?php foreach ($records as $key => $value): ?>
                            <tr>
                                <td><?php echo $value->title_1188;?></td>
                                <td><?php echo $value->email;?></td>
                                <td><?php echo $value->phone;?></td>
                                <td><?php echo $value->address;?></td>
                                <td class="text-right">
                                    <?php if($value->home_page):?>
                                        <?php echo anchor($value->home_page, '<span class="glyphicon glyphicon-home"></span>', 'class="btn btn-xs btn-warning" target="_blank"'); ?>
                                    <?php endif;?>
                                    <?php echo anchor($value->link_1188, '<span class="glyphicon glyphicon-eye-open"></span>', 'class="btn btn-xs btn-info" target="_blank"'); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="4" class="bg-warning text-center">Фирмы не найдены</td></tr>
                    <?php endif; ?>
                </table>
            </div>
            <div class="panel-footer"><?php echo $this->pagination->create_links(); ?></div>
        </div>
    </div>
</div>