<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <?php echo form_open('main/index', ['method' => 'get', 'id' => 'search-category']); ?>
        <div class="input-group">
            <?php echo form_dropdown('category', $categories, $this->input->get('category'), 'class="form-control"'); ?>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Найти фирмы</button>
            </span>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">Результаты поиска <?php echo $records['total'];?> найдено / <?php echo count($firms_ids);?> в базе</div>
                <div class="pull-right">
                    <?php if($this->input->get('category')):?>
                        <?php echo anchor('main/multiple_parse_firm/'.$this->input->get('category'),'Сохранить всю категорию','class="btn btn-warning btn-xs"');?>
                    <?php endif;?>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th class="text-right">Опции</th>
                    </tr>
                    <?php if ($records['data']): ?>
                        <?php foreach ($records['data'] as $key => $value): ?>
                            <tr>
                                <?php $in_db = in_array($value['id_1188'], $firms_ids)?true:false?>
                                <td><?php echo $in_db?'<span class="glyphicon glyphicon-ok text-success"></span>':'<span class="glyphicon glyphicon-remove text-danger"></span>'?></td>
                                <td><?php echo $value['title_1188']; ?></td>
                                <td class="text-right">
                                    <?php if(!$in_db):?>
                                    <button class="btn btn-xs btn-success save-firm" data-link="<?php echo $value['link_1188'];?>" data-category="<?php echo $this->input->get('category');?>">
                                        <span class="glyphicon glyphicon-save-file"></span>
                                    </button>
                                    <?php endif;?>
                                    <?php echo anchor($value['link_1188'], '<span class="glyphicon glyphicon-eye-open"></span>', 'class="btn btn-xs btn-info" target="_blank"'); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="3" class="bg-warning text-center">Фирмы не найдены</td></tr>
                    <?php endif; ?>
                </table>
            </div>
            <div class="panel-footer"><?php echo $this->pagination->create_links(); ?></div>
        </div>
    </div>
</div>