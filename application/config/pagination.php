<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
$config['full_tag_close'] = '</ul>';

$config['first_link'] = '<<';
$config['first_tag_open'] = '<li class="page">';
$config['first_tag_close'] = '</li>';

$config['last_link'] = '>>';
$config['last_tag_open'] = '<li class="page">';
$config['last_tag_close'] = '</li>';

$config['next_link'] = '>';
$config['next_tag_open'] = '<li class="page">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = '<';
$config['prev_tag_open'] = '<li class="page">';
$config['prev_tag_close'] = '</li>';

$config['num_tag_open'] = '<li class="page">';
$config['num_tag_close'] = '</li>';