<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Firms extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model', 'categories_m');
        $this->load->model('firms_model', 'firms_m');
    }

    public function index() {
        $count = $this->firms_m->get_firms();
        $per_page = $this->_pagination_init($count);
        $this->data['categories'] = $this->categories_m->categories_dropdown(false);
        $this->data['records'] = $this->firms_m->get_firms($per_page);
        $this->load->view($this->tpl, $this->data);
    }

    public function export() {
        $this->data['categories'] = $this->categories_m->categories_dropdown(false);
        $this->data['db_fields'] = $this->firms_m->list_fields();
        $this->load->view($this->tpl, $this->data);
    }
    
    public function export_xsl() {
        $get = $this->input->get();
        unset($get['submit']);
        $exel_data = $this->firms_m->get_exel_data($get);
        if($exel_data){
            $this->load->library('excel');
            $this->excel->generate_xls($get['db_fields'],$exel_data,'1188_firms_export');
        }else{
            $this->session->set_flashdata('error', "Данные по фирмам не найдены");
            redirect('firms/export');
        }
    }

}
