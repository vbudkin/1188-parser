<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model','categories_m');
    }

    public function index() {
        $count = $this->categories_m->count_all();
        $per_page = $this->_pagination_init($count);
        $this->data['records'] = $this->categories_m->limit($per_page,$this->uri->rsegment(3))->order_by('title')->find_all();
        $this->load->view($this->tpl, $this->data);
    }
    
    public function parse() {
        $data = $this->categories_m->parse_categories();
        if($data){
            $this->categories_m->save_parsed_categories($data);
            $this->session->set_flashdata('success', "Категории обновлены");
        }else{
            $this->session->set_flashdata('success', "Не удалось спарсить категории");
        }
        redirect('categories');
    }
    
    public function delete($id) {
        $this->categories_m->delete($id);
        $this->session->set_flashdata('error', "Запись удалена!");
        redirect($this->input->server('HTTP_REFERER'));
    }

}
