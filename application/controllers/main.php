<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model', 'categories_m');
        $this->load->model('firms_model', 'firms_m');
    }

    public function index() {
        $search = $this->input->get('category');
        $this->data['records'] = false;
        $this->data['firms_ids'] = [];
        $count = 0;
        if ($search) {
            $category = $this->categories_m->find_by('id', $search);
            $link = $this->uri->rsegment(3) ? $category->link . '?page=' . $this->uri->rsegment(3) : $category->link;
            $this->data['records'] = $this->firms_m->parse_firms_by_category($link);
            $count = $this->data['records']['total'];
            $this->data['firms_ids'] = $this->firms_m->firm_ids_by_category($search);
        }
        $this->_pagination_init($count, 20, 3, true);
        $this->data['categories'] = $this->categories_m->categories_dropdown();
        $this->assets->add(['js' => ['js/main_index.js']]);
        //$firm_data = $this->firms_m->parse_firm('http://www.1188.lv/katalogs/puratos-latvia/auglu-un-darzenu-parstrade/491140');
        //var_dump($firm_data);
        $this->load->view($this->tpl, $this->data);
    }

    public function parse_firm() {//TODO if not firm data
        $post = $this->input->post();
        $firm_data = $this->firms_m->parse_firm($post['link']);
        if ($firm_data) {
            $firm_id = $this->firms_m->save_firm($firm_data);
            $this->categories_m->save_relation($firm_id, $post['category']);
        } else {
            
        }
    }
    
    public function multiple_parse_firm() {
        set_time_limit(0);
        $category = $this->uri->rsegment(3);
        $category_data = $this->categories_m->find_by('id', $category);
        if($category && $category_data){
            $firms_ids = $this->firms_m->firm_ids_by_category($category);
            $parse_links = $this->firms_m->get_firms_links_by_category($category_data->link,$firms_ids);
            if($parse_links){
                foreach ($parse_links as $key => $value) {
                    sleep(rand(2, 5));
                    $firm_data = $this->firms_m->parse_firm($value);
                    $firm_id = $this->firms_m->save_firm($firm_data);
                    $this->categories_m->save_relation($firm_id, $category);
                }
                $total_inserted = count($parse_links);
                $this->session->set_flashdata('success', "Добалено {$total_inserted} фирм");
            }else{
                $this->session->set_flashdata('info', "Все фирмы категории {$category_data->title} уже сохранены");
            }
        }else{
             $this->session->set_flashdata('error', "Категория не найдена");
        }
        redirect($this->input->server('HTTP_REFERER'));
    }
    
}
