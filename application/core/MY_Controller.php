<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

    public $data = [];
    public $tpl = 'tpl';
    public $site_settings = [];

    public function __construct() {
        parent::__construct();
        $this->load->library(['assets', 'form_validation', 'session','pagination','curl','dom_parser']);
        $this->load->helper(['url', 'form', 'html','func']);
        $this->data['main'] = $this->main_block_loader();
        $this->assets->add(['css' => ['css/bootstrap.min.css', 'css/style.css'], 'js' => ['js/jquery-2.1.4.min.js', 'js/bootstrap.min.js']]);
        //$this->output->enable_profiler(TRUE);
    }

    public function main_block_loader() {
        $view_path = $this->router->class . '/' . $this->router->method;
        $file_path = APPPATH . 'views/' . $view_path . EXT;
        return method_exists($this->router->class, $this->router->method) && file_exists($file_path) ? $view_path : 'error_pages/error_404';
    }

    protected function _pagination_init($count, $per_page = 20, $uri_segment = 3,$use_page_numbers= false, $base_url = false) {
        $this->config->load('pagination',true);
        $config = array_merge($this->config->item('pagination'), array(
            'base_url' => $base_url ? $base_url : site_url($this->router->class . '/' . $this->router->method),
            'total_rows' => $count,
            'per_page' => $per_page,
            'num_links' => 5,
            'uri_segment' => $uri_segment,
            'use_page_numbers' => $use_page_numbers
        ));
        if (count($_GET) > 0){
             $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $config['cur_tag_open'] = '<li class="active"><a href="'.current_url().'">';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config);
        return $per_page;
    }

}
